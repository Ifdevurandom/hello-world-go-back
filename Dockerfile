FROM quay.io/libpod/alpine
USER 0
RUN apk add go
EXPOSE 8080
RUN adduser --disabled-password gouser
USER gouser
WORKDIR /home/gouser
COPY main.go /home/gouser/
CMD ["go","run", "main.go"]
